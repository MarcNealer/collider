from django.urls import path
from .views import *

urlpatterns = [
    path('', study_select, name="index"),
    path('login/', login_page, name='staff_login'),
    path('logout/', logout_page, name="logout"),
    path('study/create/', study_create, name="create_study"),
    path('study/create/save/', study_create_save, name="create_study_save"),
    path('study/select/<int:study_id>/', study_set, name="set_study"),
    path('study/edit/<int:study_id>/', study_edit, name="edit_study"),
    path('study/edit/<int:study_id>/save/', study_edit_save, name="edit_study_save"),
    path('study/delete/<int:study_id>/', study_delete, name="study_delete")
]
