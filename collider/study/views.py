from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.cache import cache
from .models import Study
# Create your views here.


def login_page(request):
    if request.user.is_authenticated:
        return redirect('/')
    message = ''
    if request.method == 'POST':
        user = authenticate(request=request, username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            message = 'Authentication failed'
    return render(request, 'login.html', {'message': message})


def logout_page(request):
    logout(request)
    return redirect('/login/')


@login_required
def study_select(request):
    """
    Lists studies so that you can set the study your currently working on
    :param request:
    :return:
    """
    user = request.user.id
    study_id = cache.get(f"current_study-{user}")
    if study_id:
        current_study = Study.objects.get(id=study_id, active=True)
    else:
        current_study = ""
    studies = Study.objects.all()
    return render(request, "study_main.html", context={"studies": studies, "current_study": current_study})


@login_required
def study_set(request, study_id):
    user = request.user.id
    cache.set(f"current_study-{user}", study_id)
    return redirect("/")


@login_required
def study_create(request):
    return render(request, "study_create.html")


@login_required
def study_create_save(request):
    rec = Study.objects.create(name=request.POST['name'],
                               slug=request.POST['name'].lower().replace(' ', '_').replace(',', '').replace("'", ""),
                               external_id=request.POST['external_id'])
    cache.set("current_study", rec.id)
    return redirect("/")


@login_required
def study_edit(request, study_id):
    study = Study.objects.get(id=study_id)
    return render(request, "study_edit.html", context={"current_study": study})


@login_required
def study_edit_save(request, study_id):
    study = Study.objects.get(id=study_id)
    study.name = request.POST['name']
    study.external_id = request.POST["external_id"]
    study.save()
    return redirect("/")


@login_required
def study_delete(request, study_id):
    current = cache.get(f"current_study{request.user.id}")
    if current == study_id:
        cache.delete(f"current_study{request.user.id}")
    study = Study.objects.get(id=study_id)
    study.active = False
    return redirect("/")

