from django.db import models
from django.core.serializers.json import DjangoJSONEncoder


class Study(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    external_id = models.CharField(max_length=100)
    visits = models.JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class StudyForms(models.Model):
    study = models.ForeignKey(Study, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    form_type = models.CharField(max_length=100, default="provider")
    procedures = models.JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
    zoho_billable_procs = models.JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Payouts(models.Model):
    study = models.ForeignKey(Study, on_delete=models.CASCADE)
    name = models.CharField(max_length=150)
    reference_code = models.SlugField(max_length=15)
    amount = models.DecimalField(max_digits=6, decimal_places=2)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class StudyJotForm(models.Model):
    study = models.ForeignKey(Study, on_delete=models.CASCADE)
    jotform_id = models.CharField(max_length=30, blank=True, null=True)
    jotform_name = models.CharField(max_length=300, blank=True, null=True)
    name = models.CharField(max_length=100)
    tags = models.CharField(max_length=200, blank=True, null=True)
    settings = models.JSONField(blank=True, null=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
