from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponseNotFound
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from study.models import Study
from .models import *


@login_required
def procedure_main(request):
    study_id = cache.get(f"current_study-{request.user.id}")
    if not study_id:
        return redirect("/")
    study = Study.objects.get(id=study_id)
    procedures = Procedure.objects.filter(study=study)
    return render(request, "procedure_main.html", {"procedures": procedures, "current_study": study})


@login_required
def procedure_create(request):
    current_study = cache.get(f"current_study-{request.user.id}")
    if not current_study:
        return redirect("/")
    return render(request, "procedure_create.html")


@login_required
def procedure_create_save(request):
    study = Study.objects.get(id=cache.get(f"current_study-{request.user.id}"))
    name = request.POST["name"].replace('-', '').replace('&', ''). replace('*', '').replace('%', '').replace('=', '')
    tab = request.POST["tab"]
    title = request.POST["title"]
    if "subtitle" in request.POST:
        subtitle = request.POST["subtitle"]
    else:
        subtitle = ""
    form = ProcedureForm.objects.create(name=name, questions=[])
    Procedure.objects.create(study=study, name=name, tab=tab,
                             title=title, subtitle=subtitle, form=form)
    return redirect('/procedures/')


@login_required
def procedure_edit(request, procedure_id):
    procedure = Procedure.objects.get(id=procedure_id)
    return render(request, "procedure_edit.html", {"procedure": procedure})


@login_required
def procedure_edit_save(request, procedure_id):
    name = request.POST["name"].replace('-', '').replace('&', ''). replace('*', '').replace('%', '').replace('=', '')
    tab = request.POST["tab"]
    title = request.POST["title"]
    if "subtitle" in request.POST:
        subtitle = request.POST["subtitle"]
    else:
        subtitle = ""
    procedure = Procedure.objects.get(id=procedure_id)
    procedure.name = name
    procedure.title = title
    procedure.tab = tab
    procedure.subtitle = subtitle
    procedure.save()
    return redirect('/procedures/')


@login_required
def procedure_form_questions(request, procedure_id):
    procedure = Procedure.objects.get(id=procedure_id)

    questions = []
    if isinstance(procedure.form.questions, list):
        for question_id in procedure.form.questions:
            questions.append(Question.objects.get(id=question_id))
    return render(request, "procedure_form.html", {"procedure": procedure, "questions": questions})


@login_required
def question_display_update(request, question_id):
    question = Question.objects.get(id=question_id)
    if question.display:
        question.display = False
        question.save()
        return JsonResponse({"result": "false"})
    else:
        question.display = True
        question.save()
        return JsonResponse({"result": "true"})


@login_required
def question_half_size_update(request, question_id):
    question = Question.objects.get(id=question_id)
    if question.half_size:
        question.half_size = False
        question.save()
        return JsonResponse({"result": "false"})
    else:
        question.half_size = True
        question.save()
        return JsonResponse({"result": "true"})


@login_required
def question_line_above_update(request, question_id):
    question = Question.objects.get(id=question_id)
    if question.line_above:
        question.line_above = False
        question.save()
        return JsonResponse({"result": "false"})
    else:
        question.line_above = True
        question.save()
        return JsonResponse({"result": "true"})


@login_required
def question_line_below_update(request, question_id):
    question = Question.objects.get(id=question_id)
    if question.line_below:
        question.line_below = False
        question.save()
        return JsonResponse({"result": "false"})
    else:
        question.line_below = True
        question.save()
        return JsonResponse({"result": "true"})


@login_required
def question_space_above_update(request, question_id):
    question = Question.objects.get(id=question_id)
    if question.space_above:
        question.space_above = False
        question.save()
        return JsonResponse({"result": "false"})
    else:
        question.space_above = True
        question.save()
        return JsonResponse({"result": "true"})


@login_required
def question_space_below_update(request, question_id):
    question = Question.objects.get(id=question_id)
    if question.space_below:
        question.space_below = False
        question.save()
        return JsonResponse({"result": "false"})
    else:
        question.space_below = True
        question.save()
        return JsonResponse({"result": "true"})
