# Generated by Django 3.2.3 on 2021-05-25 04:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('procedure', '0003_auto_20210525_0433'),
    ]

    operations = [
        migrations.AlterField(
            model_name='procedure',
            name='tab',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='procedure',
            name='title',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
