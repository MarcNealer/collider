from django.db import models
from django.core.serializers.json import DjangoJSONEncoder
# Create your models here.


class QuestionTemplate(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    include = models.CharField(max_length=100)
    parameters = models.JSONField()

    def __str__(self):
        return self.name


class Question(models.Model):
    name = models.CharField(max_length=200, unique=True)
    template = models.ForeignKey(QuestionTemplate, on_delete=models.CASCADE)
    display = models.BooleanField(default=True)
    line_above = models.BooleanField(default=False)
    line_below = models.BooleanField(default=False)
    space_above = models.BooleanField(default=False)
    space_below = models.BooleanField(default=False)
    half_size = models.BooleanField(default=False)
    values = models.JSONField()

    def __str__(self):
        return self.name


class ProcedureForm(models.Model):
    name = models.CharField(max_length=200)
    questions = models.JSONField(blank=True, null=True)

    def __str__(self):
        return self.name


class Procedure(models.Model):
    study = models.ForeignKey("study.Study", on_delete=models.CASCADE)
    name = models.CharField(max_length=200, unique=True)
    tab = models.CharField(max_length=100)
    title = models.CharField(max_length=200)
    subtitle = models.TextField(blank=True, null=True)
    form = models.ForeignKey(ProcedureForm, on_delete=models.CASCADE)
    validators = models.JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
    modifiers = models.JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
    add_form_conditionals = models.JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
    in_form_conditionals = models.JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)
    question_display = models.JSONField(encoder=DjangoJSONEncoder, blank=True, null=True)

    def __str__(self):
        return self.name
