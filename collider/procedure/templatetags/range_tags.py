from django import template
from django.utils.safestring import mark_safe
import datetime
register = template.Library()


@register.simple_tag
def years_range():
    yearnow = datetime.date.today().year
    data = [str(x) for x in range(yearnow-120, yearnow+1)]
    data.reverse()
    return data


@register.simple_tag
def years_20_range():
    yearnow = datetime.date.today().year
    return [str(x) for x in range(yearnow-20, yearnow+1)]


@register.simple_tag
def gender_range():
    return ('m', 'Male'), ('f', 'Female')


@register.simple_tag
def difficulty_range():
    diff = (('none', 'Without Difficulty'), ('some', 'Some Difficulty'),
            ('much', 'Much Difficulty'), ('unable', 'Unable to Do'))
    return diff


@register.simple_tag
def conditions_time_range():
    times = (('0', 'No'), ('0-2', 'In the last two months'), ('2-12', '2 months - 1 year ago'),
             ('12-60', '1-5 years ago'), ('60+', 'more than 5 years ago'))
    return times


@register.simple_tag
def minutes_range():
    return range(0, 61)


@register.simple_tag
def parent_range():
    data = (("father", "Father"), ("mother", "Mother"))
    return data


@register.simple_tag
def eval_range(data_string):
    return eval(data_string)


@register.simple_tag
def day_to_month_range():
    return ['Day', 'Week', 'Month']


@register.simple_tag
def yes_no_range():
    return ('yes', 'Yes'), ('no', 'No')


@register.simple_tag
def yes_no_not_sure_range():
    return ('yes', 'Yes'), ('no', 'No'), ("not sure", "I'm not_sure")


@register.simple_tag
def one_sixty_range():
    return [x for x in range(0, 60)]


@register.simple_tag
def improvement_range1():
    data_range = (('no change ot worse', 'No change or worse'), ("almost the same", 'Almost the same'),
                  ("a little better", 'A little better'), ("somewhat better", 'Somewhat better'),
                  ('moderately better', 'Moderately better'), ('better', 'Better'),
                  ('a great deal better', 'A great deal better'))
    return data_range


@register.simple_tag
def fatigue_range():
    return ('never', 'Never'), ('rarely', 'Rarely'), ('sometimes', 'Sometimes'), \
           ('often', 'Often'), ('almost always', 'Almost always')


@register.simple_tag
def how_often_range1():
    return ('not at all', 'Not at all'), ('several days', 'Several days'), \
           ('more that half the days', 'More that half the days'), ('nearly everyday', 'Nearly everyday')


@register.simple_tag
def impact_range1():
    return ('not at all', 'Not at all'), ('a little', 'A little'), ('moderatley', 'Moderately'), \
           ('quite a bit', 'Quite a bit'), ('extremely', 'Extremely')


@register.simple_tag
def months_3c():
    data = (('jan', 'Jan'), ('feb', 'Feb'), ('mar', 'Mar'), ('apr', 'Apr'), ('may', 'May'), ('jun', 'Jun'),
            ('jul', 'Jul'), ('aug', 'Aug'), ('sep', 'Sep'), ('oct', 'Oct'), ('nov', 'Nov'), ('dec', 'Dec'))
    return data


@register.simple_tag
def pathogen_codes():
    data = (('BA', "Bacterial (not MSRA)"), ('CD', "Coccidioiomycosis"), ('CY', "Cryptococcus"), ('HA', 'H. zoster/VZV'),
            ('HP', 'Histoplasmosis'),('JC', 'JC Virus (PML virus)'), ('LI', 'Listeria'), ('MR', 'MRSA'),
            ('PN', 'Pneumocystis'), ('TB', 'Mycrobacterium tuberculosis'), ('NO', "Other non-opportunistic"),
            ('OO', 'Other Opportunistic'), ('UK', 'Unknown'))
    return data


@register.simple_tag
def drug_start_codes():
    data = (
        ('AR', 'active disease (RELAPSE'),
        ('AM', 'active disease (MRI)'),
        ('AD', 'active disease (DISABILITY)'),
        ('MA', 'alternative mechanism of action'),
        ('IT', 'to improve tolerability'),
        ('CI', 'cost / co-pay'),
        ('FA', 'frequency of administration **'),
        ('RA', 'route of administration **'),
        ('OT', 'other start reason')
    )
    return data


@register.simple_tag
def drug_stop_codes():
    data = (
        ('DW', 'patient doing well'),
        ('AR', 'active disease (RELAPSE)'),
        ('AM', 'active disease (MRI)'),
        ('AD', 'active disease (DISABILITY)'),
        ('MA', 'alternative mechanism of action'),
        ('FA', 'frequency of administration **'),
        ('RA', 'route of administration **'),
        ('FE', 'fear of future side effect **'),
        ('PP', 'patient preference (other) **'),
        ('BD', 'antibody development'),
        ('ME', 'minor side effect'),
        ('SE', 'serious side effect'),
        ('TI', 'temporary interruption'),
        ('CI', 'cost / co-pay'),
        ('DI', 'denied by insurance'),
        ('BF', 'breastfeeding'),
        ('OT', 'other stop reason')
    )
    return data


@register.simple_tag
def drug_modification_codes():
    data = (
        ('NA', 'No change/No longer used'),
        ('AR', 'active disease (RELAPSE)'),
        ('AM', 'active disease (MRI)'),
        ('AD', 'active disease (DISABILITY)'),
        ('BD', 'antibody development'),
        ('ME', 'minor side effect'),
        ('SE', 'serious side effect'),
        ('OT', 'other dose modification reason')
    )
    return data


@register.simple_tag
def issue_status():
    data = (("open", "Open"), ("closed", "Closed"))
    return data


@register.simple_tag
def dmt_reason_codes():
    data = (
        ('CP', 'Co-pay/patient cost(insurance/cost)'),
        ('DI', 'Denied by insurance'),
        ('CC', 'Comorbidity or contraindication'),
        ('FE', 'Fear of future side effects'),
        ('PP', 'Patient preference'),
        ('OT', 'Other')
    )
    return data


@register.simple_tag
def american_ethnicity():
    data = (('hispanic_latino', 'Hispanic or Latino'),('not_hispanic_latino', 'Not Hispanic or Latino'))
    return data


@register.simple_tag
def american_race():
    data = (('white', 'White'), ('black', 'Black/African American'), ('asian', 'Asian'),
            ('native', 'American Indian or Alaskan Native'),
            ('pacific_islander', 'Native Hawaiian or Other Pacific Islander'), ('other', 'other'))
    return data


@register.simple_tag
def corrona_education():
    data = (('primary', 'Primary School'), ('high_school', 'High School'),
            ('university_completed', 'College/University (Completed)'),
            ('university_incomplete', 'College/University (Incomplete)'),
            ("dont_remember", "Don't Remember"))
    return data


@register.simple_tag
def corrona_marital_status():
    data = (('single', 'Single'), ('married', 'Married'), ('partnered', 'Partnered'), ('widowed', 'Widowed'),
            ('separated', 'Separated'), ('divorced', 'Divorced'))
    return data


@register.simple_tag
def corrona_ra_work_status():
    data = (('full_time', 'Full Time'), ('part_time', 'Part Time'), ('student', 'Student'),
            ('retired', 'Retired'), ('disabled_ra', 'Disabled Due to RA'), ('disabled', 'Disabled Not Due to RA'),
            ('not_working', 'Not working outside home with pay'))
    return data


@register.simple_tag
def corrona_insurance():
    data = (('private', 'Private'), ('medicare_fee', 'Medicare (Fee for Service)'), ('medicare_hmo', 'Medicare (HMO)'),
            ('medicaid', 'Medicaid'), ('none', 'No Insurance'))
    return data


@register.simple_tag
def corrona_ra_devices():
    data = (
        ('cane', 'Cane'), ('crutches', 'Crutches'), ('special_utensils', 'Built up or special utensils'),
        ('walker', 'Walker'), ('wheelchair', 'Wheelchair'), ('built_up_chair', 'Special or built up chair'),
        ('dressing_devices', 'Devices used for dressing (button, hook, zipper pull, etc.'),
        ('raise_toilet', 'Raise Toilet seat'), ('bathtub_bar', 'Bathtub bar'), ('bathtub_seat', 'Bathtub seat'),
        ('long_handled_reach', 'Long-handled appliances for reach'),
        ('long_handled_bathroom', 'Long-handled appliances in bathroom'),
        ('jar_opener', 'Jar opener (for jars previously opened')
    )

    return data


@register.simple_tag
def corrona_ra_help_needed():

    data = (
        ('dressing_and_grooming', 'Dressing and Grooming'),
        ('arising', 'Arising'), ('eating', 'Eating'), ('walking', 'Walking'),
        ('hygiene', 'Hygiene'), ('reach', 'Reach'), ('opening_things', 'Gripping and opening things'),
        ('chores', 'Errands and chores')
    )
    return data


@register.simple_tag
def corrona_ra_medications():
    data = (
        ('clopidogrel_plavix', 'clopidogrel / plavix'), ('warfarin_coumadin', 'warfarin / coumadin'),
        ('diabetes', 'diabetes medication(s)'), ('angina_nitrate', 'angina /nitrate medications'),
        ('lipid_lowering', 'lipid (cholesterol) lowering medication(s)'),
        ('blood_pressure_lowering', 'blood pressure lowering medication(s)'),
        ('lyrica', 'lyrica'), ('folic_acid', 'folic acid'),
        ('reflux_peptic_ulcer', 'medication(s) for reflux or to prevent peptic ulcer disease'),
        ('tramadol', 'tramadol (ex. ultram)'), ('antidepressant', 'antidepressant medication(s)'),
        ('narcotic_pain', 'narcotic pain medication(s) (ex. lortab, vicodin, Percocet)'),
        ('other_pain', 'other pain medication(s) / analgesics'))

    return data


@register.simple_tag
def corrona_ra_alcohol_consumption():
    data = []
    for x in range(0, 21):
        data.append([f"{x}d", f"{x} per day"])
    for x in range(0, 41):
        data.append([f"{x}m", f"{x} per month"])
    for x in range(1, 41):
        data.append([f"{x}y", f"{x} per year"])
    return data


@register.simple_tag
def number_range_with_none_not_sure():
    data = [str(x) for x in range(1, 60)]
    data.insert(0, 'not sure')
    data.insert(0, 'none')
    return data


@register.simple_tag
def mc_merge_ranges(list1,list2, list3=None):
    new_range = []
    for x in range(0,len(list1)):
        try:
            if not list3:
                new_range.append([list1[x], list2[x], ''])
            else:
                new_range.append([list1[x], list2[x], list3[list1[x]]])
        except:
            pass
    return new_range


@register.simple_tag
def number_range(first, last):
    return range(first, last)


@register.simple_tag
def split_key_val(item):
    key, val = item.split(":", 1)
    return key.strip(), val.strip()
