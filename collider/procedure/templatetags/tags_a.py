from django import template
from django.utils.safestring import mark_safe
import datetime
register = template.Library()


@register.filter(needs_autoescape=True)
def he_button_group(data_name, autoescape=True):
    template = """                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                  <label class="btn btn-secondary active">
                                    <input type="radio" name="{}" required value="none" checked>Without Difficulty
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="some">Some Difficulty
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="much" autocomplete="off">Much Difficulty
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="unable" autocomplete="off">Unable To Do
                                  </label>
                                </div>
    """.format(data_name, data_name, data_name, data_name)
    return mark_safe(template)


@register.filter(needs_autoescape=True)
def he_conditions_buttons(data_name, autoescape=True):
    template = """            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                  <label class="btn btn-secondary active">
                                    <input type="radio" name="{}" required value="0" checked>No
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="0-2">In the last 2 months
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="2-12" autocomplete="off">2 months - 1 year ago
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="12-60" autocomplete="off">1-5 years ago
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="60+1" autocomplete="off">1-5 years ago
                                  </label>
                                </div>
    """.format(data_name, data_name, data_name, data_name, data_name)
    return mark_safe(template)


@register.simple_tag
def years_range():
    yearnow = datetime.date.today().year
    return range(yearnow-80, yearnow)

@register.simple_tag
def gender_range():
    return ('m', 'Male'), ('f', 'Female')
