from django import template
from django.template import Template
from django.utils.safestring import mark_safe
from django.template import Context
import datetime
register = template.Library()


@register.filter(needs_autoescape=True)
def he_button_group(data_name, autoescape=True):
    template = """             <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                  <label class="btn btn-secondary active">
                                    <input type="radio" name="{}" required value="none" checked>Without Difficulty
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="some">Some Difficulty
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="much" autocomplete="off">Much Difficulty
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="unable" autocomplete="off">Unable To Do
                                  </label>
                                </div>
    """.format(data_name, data_name, data_name, data_name)
    return mark_safe(template)


@register.filter(needs_autoescape=True)
def he_conditions_buttons(data_name, autoescape=True):
    template = """            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                  <label class="btn btn-secondary active">
                                    <input type="radio" name="{}" required value="0" checked>No
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="0-2">In the last 2 months
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="2-12" autocomplete="off">2 months - 1 year ago
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="12-60" autocomplete="off">1-5 years ago
                                  </label>
                                  <label class="btn btn-secondary">
                                    <input type="radio" name="{}" value="60+1" autocomplete="off">1-5 years ago
                                  </label>
                                </div>
    """.format(data_name, data_name, data_name, data_name, data_name)
    return mark_safe(template)


@register.simple_tag
def proc_include(proc_template, results, *args, **kwargs):
    html = proc_template
    tmpl = Template(html)
    return mark_safe(tmpl.render(Context({'results': results})))


@register.simple_tag
def get_result(item, results=None):
    if not results:
        return ''
    if item in results.keys():
        return results[item]
    else:
        return ''


@register.simple_tag
def get_result_column(item, column, results=None):
    if not results:
        return ''
    field = f"{item}_{column}"
    if field in results.keys():
        return results[field]
    else:
        return ''


@register.simple_tag
def convert_list_for_edit(item):
    if isinstance(item, list):
        return "\r\n".join(item)
    else:
        return item


@register.simple_tag
def convert_updates_for_edit(updates):
    return "\r\n".join([" ".join(x) for x in updates])


@register.simple_tag
def linesplit(value):
    return [mark_safe(x) for x in value.split('*&*')]


@register.simple_tag
def validators_to_select_values(value):
    return sorted([[x[0],x[0]] for x in value], key=lambda x: x[1])


@register.simple_tag
def display_questions_to_select_values(value):
    return sorted([[x[1], x[0]] for x in value], key=lambda x: x[1])


@register.simple_tag
def join_values(list1, list2):
    return list(zip(list1, list2))


@register.simple_tag
def grid_name(grid, index, field):
    return f"{grid}[{index}]{field}"


@register.filter
def counter0(item):
    count = len(item)-1
    if count < 0:
        return 0
    else:
        return count
