from django import template
from django.utils.safestring import mark_safe
import datetime
register = template.Library()


@register.filter()
def cap_words(value):
    return ' '.join([t.title() for t in value.split('_')])


@register.filter()
def osm_slug(value):
    return value.replace(' ', '_').lower()
