from django.urls import path
from .views import *

urlpatterns = [
    path('', procedure_main, name="procedure_main"),
    path('create/', procedure_create, name="procedure_create"),
    path('create/save/', procedure_create_save, name="procedure_create_save"),
    path('edit/<int:procedure_id>/', procedure_edit, name="procedure_edit"),
    path('edit/<int:procedure_id>/save/', procedure_edit_save, name="procedure_edit_save"),
    path('form/<int:procedure_id>/questions/', procedure_form_questions, name="procedure_form_questions"),
    path('form/display/<int:question_id>/update/', question_display_update, name="question_display_update"),
    path('form/line_above/<int:question_id>/update/', question_line_above_update, name="question_line_above_update"),
    path('form/line_below/<int:question_id>/update/', question_line_below_update, name="question_line_below_update"),
    path('form/space_above/<int:question_id>/update/', question_space_above_update, name='question_space_above_update'),
    path('form/space_below/<int:question_id>/update/', question_space_below_update, name="question_space_below_update"),
    path('form/half_size/<int:question_id>/update/', question_half_size_update, name='question_half_size_update')
]
