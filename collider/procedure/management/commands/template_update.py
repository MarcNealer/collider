from django.core.management.base import BaseCommand
import os
from procedure.models import QuestionTemplate


class Command(BaseCommand):
    help = 'Import app made in OFD'

    def handle(self, *args, **options):
        files = os.listdir('procedure/templates/question_templates')
        for file in files:
            if file[-5:] == '.html':
                f = open(f"procedure/templates/question_templates/{file}", "r")
                record = {"name": "", "parameters": [], "description": ""}
                params = False
                for line in f:
                    if params:
                        if "name:" in line:
                            record["name"] = line.split("name:")[1].strip()
                        elif "parameter:" in line:
                            p = line.split("parameter:")[1].strip()
                            record["parameters"].append(p.split(" "))
                        elif "description:" in line:
                            record["description"] = record["description"] + line.split("description:")[1].strip()

                    elif "{% comment %}" in line:
                        params = True
                    elif "endcomment" in line:
                        params = False
                if record["name"]:
                    if QuestionTemplate.objects.filter(include=file).exists():
                        rec = QuestionTemplate.objects.get(include=file)
                        rec.name = record["name"]
                        rec.parameters = record["parameters"]
                        rec.description = record["description"]
                        rec.save()
                        print(f"{rec.name} template updated")
                    else:
                        QuestionTemplate.objects.create(name=record["name"], include=file,
                                                        parameters=record["parameters"],
                                                        description=record["description"])
                        print(f"{record['name']} template created")



